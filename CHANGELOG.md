# Tropicalish Color Theme - Change Log

## [0.1.4]

- update screenshot / readme to indicate add'l supported apps
- canonical layout reorg and fix image links

## [0.1.3]

- update readme and screenshot

## [0.1.2]

- fix source control graph badge colors: brighten scmGraph.historyItemRefColor
- fix titlebar border, FG/BG in custom mode
- define window border in custom mode

## [0.1.1]

- fix manifest and pub WF
- fix pkg

## [0.1.0]

- dimmer buttons BG so as not to compete visually with project names in Git sidebar
- lighter BG on tab/breadcrumb/statusbar
- fix manifest repo links
- retain v0.0.1 for those that prefer earlier style

## [0.0.1]

- Initial release
