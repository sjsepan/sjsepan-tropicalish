# Tropicalish Theme

Tropicalish color theme for Code-OSS based apps (VSCode, Codium, code.dev, AzureDataStudio, TheiaIDE and Positron).

Created by sjsepan.

**Enjoy!**

VSCode:
![./images/sjsepan-tropicalish_code.png](./images/sjsepan-tropicalish_code.png?raw=true "VSCode")
Codium:
![./images/sjsepan-tropicalish_codium.png](./images/sjsepan-tropicalish_codium.png?raw=true "Codium")
Code.Dev:
![./images/sjsepan-tropicalish_codedev.png](./images/sjsepan-tropicalish_codedev.png?raw=true "Code.Dev")
Azure Data Studio:
![./images/sjsepan-tropicalish_ads.png](./images/sjsepan-tropicalish_ads.png?raw=true "Azure Data Studio")
TheiaIDE:
![./images/sjsepan-tropicalish_theia.png](./images/sjsepan-tropicalish_theia.png?raw=true "TheiaIDE")
Positron:
![./images/sjsepan-tropicalish_positron.png](./images/sjsepan-tropicalish_positron.png?raw=true "Positron")

## Contact

Steve Sepan

<sjsepan@yahoo.com>

1/13/2025
